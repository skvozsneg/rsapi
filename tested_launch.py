from rsapi import Rsa

MESSAGE = "You were here, weren't you? Little pig."

if __name__ == '__main__':
    rsa = Rsa()

    sign = rsa.sign_message(MESSAGE)

    if rsa.check_message(MESSAGE, sign):
        print("It's from Chris Walker...")
    else:
        print("It is not from Chris Walker. But can't calm already.")

