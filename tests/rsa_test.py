import unittest

from random import randint

import tests.utils as utils

from rsapi import Rsa
from rsapi import WrongVarRange


class RsaTests(unittest.TestCase):
    def setUp(self):
        self.rsa = Rsa()

    # -- good cases --
    def test_check_digit_for_prime(self):
        """Проверка ф-ции валидации простого числа."""
        var = 9941

        self.assertTrue(self.rsa.is_prime(var))

    def test_check_random_primes(self):
        """Проверка генерации случайных простых чисел."""
        n = randint(1, 10)
        primes = self.rsa.get_random_primes(
            min_prime=2,
            max_prime=1000,
            n=n
        )

        self.assertLessEqual(len(primes), n)

    def test_message_signed_message_20_len(self):
        """Проверка подписи случаайного сообщения длиной 20."""
        mes_len = 20
        tested_message = utils.generate_random_message(mes_len)
        sign = self.rsa.sign_message(tested_message)

        self.assertTrue(self.rsa.check_message(tested_message, sign))

    def test_message_signed_message_100_len(self):
        """Проверка подписи случаайного сообщения длиной 100."""
        mes_len = 100
        tested_message = utils.generate_random_message(mes_len)
        sign = self.rsa.sign_message(tested_message)

        self.assertTrue(self.rsa.check_message(tested_message, sign))

    def test_message_signed_message_500_len(self):
        """Проверка подписи случаайного сообщения длиной 500."""
        mes_len = 500
        tested_message = utils.generate_random_message(mes_len)
        sign = self.rsa.sign_message(tested_message)

        self.assertTrue(self.rsa.check_message(tested_message, sign))

    def test_message_signed_message_1000_len(self):
        """Проверка подписи случаайного сообщения длиной 1000."""
        mes_len = 1000
        tested_message = utils.generate_random_message(mes_len)
        sign = self.rsa.sign_message(tested_message)

        self.assertTrue(self.rsa.check_message(tested_message, sign))

    def test_message_signed_message_10000_len(self):
        """Проверка подписи случаайного сообщения длиной 10000."""
        mes_len = 10000
        tested_message = utils.generate_random_message(mes_len)
        sign = self.rsa.sign_message(tested_message)

        self.assertTrue(self.rsa.check_message(tested_message, sign))

    # -- bad cases --
    def test_wrong_var_range(self):
        """Вызов исключения на неверные диапазоны."""
        with self.assertRaises(WrongVarRange):
            self.rsa.get_random_primes(
                min_prime=100,
                max_prime=10,
                n=0
            )
