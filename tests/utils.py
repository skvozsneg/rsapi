import string
import secrets


def generate_random_message(length):
    """Генерирование случайной строки из букв цифр и пробелов.

    :param length: Длина строки.
    :type length: int
    :rtype: str
    :return: Случайная строка.
    """
    data = string.ascii_letters + string.digits + string.whitespace
    res = ''.join(secrets.choice(data) for _ in range(length))

    return res
